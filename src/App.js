import { useState, useEffect } from "react";
import "./App.css";
import CardList from "./components/card-list/card-list.component";
import SearchBox from "./components/searchBox/searchBox.component";

const App = () => {
  const [searchField, setSearchField] = useState("");
  const [monsters, setmonsters] = useState([]);
  const [filterMonsters, setfilterMonsters] = useState(monsters);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((users) => setmonsters(users));
  }, []);

  useEffect(() => {
    const newFilterMonsters = monsters.filter((monster) => {
      return monster.name.toLocaleLowerCase().includes(searchField);
    });

    setfilterMonsters(newFilterMonsters);
  }, [monsters, searchField]);

  const onSearchChange = (event) => {
    const searchString = event.target.value.toLocaleLowerCase();
    setSearchField(searchString);
  };

  return (
    <div className="App">
      <h1 className="app-title">Monsters Search</h1>

      <SearchBox
        onChangerHandler={onSearchChange}
        className="search-monsters"
        placeholder="Search Monster"
      />
      <CardList elementsList={filterMonsters} />
    </div>
  );
};

export default App;
