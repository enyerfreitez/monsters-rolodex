import "./search-box.styles.css";

const SearchBox = ({ className, placeholder, onChangerHandler }) => (
  <>
    <input
      type="search"
      className={`search-box ${className}`}
      placeholder={placeholder}
      onChange={onChangerHandler}
    />
  </>
);

export default SearchBox;
