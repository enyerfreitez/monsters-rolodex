import "./card-list.styles.css";
import Card from "../card/card.componet";

const CardList = ({ elementsList }) => (
  <div className="card-list">
    {elementsList.map((element) => {
      return <Card element={element} />;
    })}
  </div>
);

export default CardList;
