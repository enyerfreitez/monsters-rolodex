import { Component } from "react";
import "./App.css";
import CardList from "./components/card-list/card-list.component";
import SearchBox from "./components/searchBox/searchBox.component";
/** Clas Component
 *
 * this is another way of represent the components. Is necessary extents of the class react the function Component.
 *  For render is necessary use the function render and in the return the information in view
 *
 *  it is necessary to use the constructor of the class and call the super() function  which allows access to react information
 *  this way we can call the state attribute and assign a value
 *
 *  to be able to use the operation of the useEffect in of class components. setState() is passed 2 callback functions.
 *  The first is responsible for updating the information. It is necessary to return the data.
 *  In the second function. The actions to perform are sent after the state is updated
 *
 *  componentDidMount() the code that you want to execute is placed after the component has been mounted.
 *
 *  toLocaleLowerCase() funtion of JS to convert all text to lowercase
 *
 *  filter() JS function that iterates through each element and compares a statement and decides that it does. upon completion he returns it
 *
 *  includes() JS function that verific it the string contains the character
 */

class App extends Component {
  constructor() {
    super();
    this.state = {
      monsters: [],
      searchString: "",
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((users) => {
        this.setState(() => {
          return { monsters: users };
        });
      });
  }

  onSearchChange = (event) => {
    const searchString = event.target.value.toLocaleLowerCase();
    this.setState(() => {
      return { searchString };
    });
  };

  render() {
    const { monsters, searchString } = this.state;
    const { onSearchChange } = this;

    const filterMonsters = monsters.filter((monster) =>
      monster.name.toLocaleLowerCase().includes(searchString)
    );

    return (
      <div className="App">
        <h1 className="app-title">Monsters Search</h1>
        <SearchBox
          onChangerHandler={onSearchChange}
          className="search-monsters"
          placeholder="Search Monster"
        />
        <CardList elementsList={filterMonsters} />
      </div>
    );
  }
}

export default App;
